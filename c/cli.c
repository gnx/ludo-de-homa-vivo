#include<stdio.h> // за printf
#include<stdlib.h> // за malloc
#include<stdbool.h> // за bool

int _counter = 0;

struct Cell {
	int id;
	struct Cell* neig[6];
};

struct Cell* createCell() {
	struct Cell* c = (struct Cell*) malloc(sizeof(struct Cell));
	c->id = _counter;
	_counter++;
	return c;
}

int next(int a) {
	return (a + 1) % 6;
}

int prev(int a) {
	return (a + 5) % 6;
}

void createChildren(struct Cell* this) {
	int current;
	
	for (int i = 0; i < 6; i++) {
		if (!this->neig[i]) {
			this->neig[i] = createCell();
			this->neig[i]->neig[next(next(next(i)))] = this;
		}
	}
	
	for (int i = 0; i < 6; i++) {
		this->neig[i]->neig[next(next(i))] = this->neig[next(i)];
		this->neig[i]->neig[prev(prev(i))] = this->neig[prev(i)];
	}
}

void populateLevels(struct Cell* init, int levelsLeft) {
	if (levelsLeft < 1) {
		return;
	}
	createChildren(init);
	for (int i = 0; i < 6; i++) {
		populateLevels(init->neig[i], levelsLeft - 1);
	}
}

int main() {
	struct Cell* init = createCell();
	populateLevels(init, 2);
	printf("%d\n", init->neig[0]->neig[2]->neig[1]->id);
	printf("%d\n", sizeof(struct Cell*));
  return 0;
}