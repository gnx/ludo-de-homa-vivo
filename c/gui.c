#include<stdio.h>
#include<stdbool.h>
#include<SDL2/SDL.h>

int main() {
    SDL_Init(SDL_INIT_VIDEO);
    SDL_Window* window = SDL_CreateWindow("Hi", 100, 100, 600, 600, SDL_WINDOW_SHOWN);
    SDL_Event event;
    bool gameIsRunning = true;
    while(gameIsRunning) {
        SDL_PollEvent(&event);
        if(event.type == SDL_QUIT) {
            gameIsRunning = false;
        }
    }
    return 0;
}
