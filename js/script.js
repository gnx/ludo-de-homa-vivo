class Complex {
  constructor(a, b) {
    this.a = a;
    this.b = b;
  }
  
  get copy() {
    return new Complex(this.a, this.b);
  }
  
  static angular(degrees) {
    const radians = degrees * Math.PI / 180;
    const real = Math.cos(radians);
    const imag = Math.sin(radians);
    return new Complex(real, imag);
  }
  
  add(that) {
    this.a += that.a;
    this.b += that.b;
  }
  
  multiply(that) {
    const a = this.a;
    const b = this.b;
    const c = that.a;
    const d = that.b;
    this.a = a * c - b * d;
    this.b = a * d + b * c;
  }
  
  rotated(degree) {
    const other = Complex.angular(degree);
    const copy = this.copy;
    copy.multiply(other);
    return copy;
  }
  
  distanceTo(that) {
    const dx = Math.abs(this.a - that.a);
    const dy = Math.abs(this.b - that.b);
    return Math.sqrt(dx * dx + dy * dy);
  }
}

class Cell {
  constructor(x, y, field) {
    this.pos = new Complex(x, y);
    this.field = field;
    this.isAlive = false;
    this.isFemale = false;
  }
  
  get x() {
    return this.pos.a;
  }
  
  get y() {
    return this.pos.b;
  }
  
  get r() {
    return 10;
  }
  
  get copy() {
    return new Cell(this.x, this.y, this.field);
  }
  
  get circle() {
    const circle = document.createElementNS("http://www.w3.org/2000/svg", 'circle');
    const translated = this.translated(this.field.w / 2, this.field.h / 2);
    const mirrored = translated.mirroredVertically;
    circle.setAttribute("cx", mirrored.x);
    circle.setAttribute("cy", mirrored.y);
    circle.setAttribute("r", this.r);
    circle.setAttribute("fill", "white");
    circle.setAttribute("stroke", "black");
    circle.setAttribute("stroke-width", "1");
    return circle;
  }
  
  get right() {
    return new Cell(this.x + 2 * this.r, this.y, this.field);
  }
  
  get mirroredVertically() {
    return new Cell(this.x, this.field.h - this.y);
  }
  
  get neighbours() {
    const arr = [];
    arr.push(this.right);
    arr.push(arr[0].rotated(60));
    arr.push(arr[1].rotated(60));
    arr.push(arr[2].rotated(60));
    arr.push(arr[3].rotated(60));
    arr.push(arr[4].rotated(60));
    return arr;
  }
  
  rotated(degree) {
    const copy = this.copy;
    copy.pos = copy.pos.rotated(degree);
    return copy;
  }
  
  translated(x, y) {
    const copy = this.copy;
    const base = new Complex(x, y);
    copy.pos.add(base);
    return copy;
  }
  
  distanceTo(that) {
    return this.pos.distanceTo(that.pos);
  }
  
  isNeighbourOf(that) {
    return Math.abs(this.distanceTo(that) - 2 * this.r) < 1;
  }
}

class Field {
  constructor(svg) {
    this.svg = svg;
    this.initCells();
    this.draw();
    this.printCells();
  }
  
  get w() {
    return Number(svg.getAttribute("width"));
  }
  
  get h() {
    return Number(svg.getAttribute("height"));
  }
  
  initCells() {
    this.cells = [];
    this.cells.push(new Cell(0, 0, this));
    const neighbours = this.cells[0].neighbours;
    for (let cell of neighbours) {
      this.cells.push(cell);
    }
  }
  
  draw() {
    for (let cell of this.cells) {
      svg.appendChild(cell.circle);
    }
  }
  
  printCells() {
    for (let cell of this.cells) {
      console.log(cell);
    }
  }
}

const svg = document.querySelector("svg");
const field = new Field(svg);